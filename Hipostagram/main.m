//
//  main.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 01/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
