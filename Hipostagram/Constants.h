//
//  Constants.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 06/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#ifndef Hipostagram_Constants_h
#define Hipostagram_Constants_h

#define collectionFetchItemCount 18
#define defaultSearchTag @"nofilter"

#endif
