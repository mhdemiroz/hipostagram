//
//  HGPhotosViewController.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 05/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramKit.h"
#import "PureLayout.h"
#import "HGCollectionViewCell.h"
#import "HGPhotoDetailViewController.h"
#import "Constants.h"
#import "CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h"

@interface HGPhotosViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UISearchBarDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (assign, nonatomic) CGRect selectedCellFrame;

@end
