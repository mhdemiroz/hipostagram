//
//  AppDelegate.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 01/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HGPhotosViewController.h"
#import "NavigationControllerDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) NavigationControllerDelegate *navigationControllerDelegate;

@end

