//
//  CircleTransitionAnimator.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 07/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HGPhotosViewController.h"
#import "HGCollectionViewCell.h"

@interface CircleTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
