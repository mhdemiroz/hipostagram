//
//  HGPhotosViewController.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 05/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import "HGPhotosViewController.h"

@interface HGPhotosViewController ()

@property (strong, nonatomic) UICollectionView *photosCollectionView;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UITableView *recentSearchesTable;
@property (strong, nonatomic) NSLayoutConstraint *recentSearchesTableHeightConstraint;
@property (strong, nonatomic) NSString *lastSearchTag;
@property (strong, nonatomic) NSMutableArray *recentSearchItems;
@property (strong, nonatomic) UIRefreshControl *bottomRefreshControl;
@property (strong, nonatomic) UIRefreshControl *topRefreshControl;
@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (strong, nonatomic) InstagramEngine *engine;
@property (strong, nonatomic) InstagramPaginationInfo *paginationInfo;
@property (strong, nonatomic) NSMutableArray *mediaArray;

@end

@implementation HGPhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    self.tapRecognizer.delegate = self;
    [self.tapRecognizer setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:self.tapRecognizer];
    
    self.title = @"Public Media";
    self.navigationController.navigationBar.backgroundColor = [UIColor darkGrayColor];
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.mediaArray = [[NSMutableArray alloc] init];
    
    // Instagram Engine
    self.engine = [InstagramEngine sharedEngine];
    self.paginationInfo = nil;
    
    // Search Bar
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, 50)];
    self.searchBar.placeholder = @"Search";
    self.searchBar.delegate = self;
    [self.view addSubview:self.searchBar];
    [self.searchBar autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0, 0, 0) excludingEdge:ALEdgeBottom];
    [self.searchBar autoSetDimensionsToSize:self.searchBar.frame.size];
    
    // Collection View
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
    self.photosCollectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flowLayout];
    self.photosCollectionView.dataSource = self;
    self.photosCollectionView.delegate = self;
    [self.photosCollectionView registerClass:[HGCollectionViewCell class] forCellWithReuseIdentifier: @"PhotoCell"];
    [self.photosCollectionView setBackgroundColor:[UIColor lightGrayColor]];
    [self.view addSubview:self.photosCollectionView];
    [self.photosCollectionView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge: ALEdgeTop];
    [self.photosCollectionView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.searchBar];
    [self updateCollectionViewLayout];
    
    // Recent Searches Table
    self.recentSearchesTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0) style:UITableViewStylePlain];
    self.recentSearchesTable.dataSource = self;
    self.recentSearchesTable.delegate = self;
    [self.recentSearchesTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:self.recentSearchesTable];
    [self.recentSearchesTable autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.searchBar];
    [self.recentSearchesTable autoSetDimension:ALDimensionWidth toSize:self.recentSearchesTable.frame.size.width];
    self.recentSearchesTableHeightConstraint = [self.recentSearchesTable autoSetDimension:ALDimensionHeight toSize:self.recentSearchesTable.frame.size.height];
    self.recentSearchesTable.bounces = NO;
    self.recentSearchItems = [[NSMutableArray alloc] init];
    
    // Refresh Controls
    self.topRefreshControl = [[UIRefreshControl alloc] init];
    self.topRefreshControl.triggerVerticalOffset = 100;
    self.topRefreshControl.tag = 0;
    [self.topRefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.photosCollectionView addSubview:self.topRefreshControl];
    
    self.bottomRefreshControl = [[UIRefreshControl alloc] init];
    self.bottomRefreshControl.triggerVerticalOffset = 100;
    self.bottomRefreshControl.tag = 1;
    [self.bottomRefreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.photosCollectionView.bottomRefreshControl = self.bottomRefreshControl;
    
    // Populate collection
    [self loadMediaWithTag:defaultSearchTag];
    
    [self.recentSearchesTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Gesture Recognition

- (void)hideKeyboard {
    self.searchBar.text = @"";
    [self.searchBar endEditing:true];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view.superview isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark Refresh Control

- (void)refresh:(UIRefreshControl *)sender {
    if (sender.tag == 0) {
        [self.mediaArray removeAllObjects];
        [self loadMediaWithTag:defaultSearchTag];
    } else {
        [self loadMediaWithTag:self.lastSearchTag];
    }
}

#pragma mark API Requests

- (void)loadMediaWithTag:(NSString *)tag {
    [self.engine getMediaWithTagName:tag count:collectionFetchItemCount maxId:self.paginationInfo.nextMaxId withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        if (![tag isEqualToString:self.lastSearchTag]) {
            [self.mediaArray removeAllObjects];
        }
        self.lastSearchTag = tag;
        [self.mediaArray addObjectsFromArray:media];
        [self.photosCollectionView.bottomRefreshControl endRefreshing];
        [self.topRefreshControl endRefreshing];
        [self.photosCollectionView reloadData];
        self.paginationInfo = paginationInfo;
        if ([self.lastSearchTag isEqualToString:defaultSearchTag]) {
            self.title = @"Public Media";
        } else {
            self.title = [NSString stringWithFormat:@"#%@", self.lastSearchTag];
        }
        if ([self.mediaArray count] == 0) {
            UIAlertView *invalidTagAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No media found with that tag." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [invalidTagAlert show];
        }
    } failure:^(NSError *error, NSInteger serverStatusCode) {
        NSLog(@"%@", error.localizedDescription);
    }];
}

#pragma mark Collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.mediaArray.count;
}

- (HGCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = cell.frame.size.width / 2;
    cell.layer.masksToBounds = YES;
    [cell setImageURL:((InstagramMedia *)self.mediaArray[indexPath.row]).thumbnailURL];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    HGPhotoDetailViewController *photoDetailViewController = [[HGPhotoDetailViewController alloc] init];
    InstagramMedia *selectedMedia = self.mediaArray[indexPath.row];
    photoDetailViewController.creatorImageURL = selectedMedia.user.profilePictureURL;
    photoDetailViewController.creatorName = selectedMedia.user.username;
    photoDetailViewController.createdDate = selectedMedia.createdDate;
    photoDetailViewController.photoURL = selectedMedia.standardResolutionImageURL;    
    HGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    self.selectedCellFrame = [self.photosCollectionView convertRect:cell.frame toView:self.view];
    [self.navigationController pushViewController:photoDetailViewController animated:true];
}

- (void)updateCollectionViewLayout {
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.photosCollectionView.collectionViewLayout;
    CGFloat size = floor((CGRectGetWidth(self.photosCollectionView.bounds) - 1) / 4);
    layout.itemSize = CGSizeMake(size, size);
}

#pragma mark Table View

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (indexPath.row == [self.recentSearchItems count]) {
        cell.textLabel.text = @"Clear Recent";
        [cell.textLabel setFont:[UIFont boldSystemFontOfSize:16]];
    } else {
        cell.textLabel.text = self.recentSearchItems[indexPath.row];
        [cell.textLabel setFont:[UIFont systemFontOfSize:16]];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.recentSearchItems count] > 0) {
        return [self.recentSearchItems count] + 1;
    }
    return [self.recentSearchItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [self.recentSearchItems count]) {
        [self clearRecent];
    } else {
        self.searchBar.text = self.recentSearchItems[indexPath.row];
        [self.searchBar resignFirstResponder];
        [self setRecentSearchesTableHeight:0];
        if (![self.searchBar.text isEqualToString:self.lastSearchTag]) {
            self.paginationInfo = nil;
        }
        [self loadMediaWithTag:self.searchBar.text];
    }
    [self.recentSearchesTable deselectRowAtIndexPath:indexPath animated:YES];
    self.searchBar.text = @"";
}

#pragma mark Search Bar

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self setRecentSearchesTableHeight:0];
    if (![searchBar.text isEqualToString:self.lastSearchTag]) {
        self.paginationInfo = nil;
    }
    [self loadMediaWithTag:self.searchBar.text];
    [self addToRecentSearches:self.searchBar.text];
    self.searchBar.text = @"";
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    if ([self.recentSearchItems count] == 0) {
        [self setRecentSearchesTableHeight:0];
    } else {
        [self setRecentSearchesTableHeight:((int)[self.recentSearchItems count] + 1) * 40];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self setRecentSearchesTableHeight:0];
}

- (void)setRecentSearchesTableHeight:(int)height {
    self.recentSearchesTableHeightConstraint.constant = height;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)addToRecentSearches:(NSString *)searchedTag {
    [self.recentSearchItems insertObject:searchedTag atIndex:0];
    if ([self.recentSearchItems count] > 4) {
        [self.recentSearchItems removeObjectAtIndex:2];
    }
    [self.recentSearchesTable reloadData];
}

- (void)clearRecent {
    [self.recentSearchItems removeAllObjects];
    [self.recentSearchesTable reloadData];
    [self setRecentSearchesTableHeight:0];
}

#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self loadMediaWithTag:defaultSearchTag];
}
@end
