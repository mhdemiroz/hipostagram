//
//  HGCollectionViewCell.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 05/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "PureLayout.h"

@interface HGCollectionViewCell : UICollectionViewCell

- (void)setImageURL:(NSURL *)imageURL;

@end
