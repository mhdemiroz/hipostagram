//
//  CircleTransitionAnimator.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 07/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import "CircleTransitionAnimator.h"

@interface CircleTransitionAnimator()

@property (weak, nonatomic) id<UIViewControllerContextTransitioning> context;

@end

@implementation CircleTransitionAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.75;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    self.context = transitionContext;
    UIView *containerView = [transitionContext containerView];
    HGPhotosViewController *fromViewController = (HGPhotosViewController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [containerView addSubview:toViewController.view];
    CGRect transitionOriginFrame = fromViewController.selectedCellFrame;
    
    UIBezierPath *circleMaskPathInitial = [UIBezierPath bezierPathWithOvalInRect:transitionOriginFrame];
    CGPoint extremePoint = CGPointMake(transitionOriginFrame.origin.x + transitionOriginFrame.size.width / 2, (transitionOriginFrame.origin.y + transitionOriginFrame.size.height / 2) + CGRectGetHeight(toViewController.view.bounds));
    float radius = sqrtf((extremePoint.x * extremePoint.x) + (extremePoint.y * extremePoint.y));
    UIBezierPath *circleMaskPathFinal = [UIBezierPath bezierPathWithOvalInRect:CGRectInset(transitionOriginFrame, -radius, -radius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = circleMaskPathFinal.CGPath;
    toViewController.view.layer.mask = maskLayer;
    
    CABasicAnimation *maskLayerAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    maskLayerAnimation.fromValue = (__bridge id)(circleMaskPathInitial.CGPath);
    maskLayerAnimation.toValue = (__bridge id)(circleMaskPathFinal.CGPath);
    maskLayerAnimation.duration = [self transitionDuration:transitionContext];
    maskLayerAnimation.delegate = self;
    [maskLayer addAnimation:maskLayerAnimation forKey:@"path"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (self.context != nil) {
        [self.context completeTransition:![self.context transitionWasCancelled]];
        [self.context viewControllerForKey:UITransitionContextFromViewControllerKey].view.layer.mask = nil;
    }
}

@end
