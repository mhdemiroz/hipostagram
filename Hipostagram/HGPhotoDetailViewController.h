//
//  HGPhotoDetailViewController.h
//  Hipostagram
//
//  Created by Hakan Demiroz on 06/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "PureLayout.h"

@interface HGPhotoDetailViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) NSURL *creatorImageURL;
@property (strong, nonatomic) NSString *creatorName;
@property (strong, nonatomic) NSDate *createdDate;
@property (strong, nonatomic) NSURL *photoURL;

@end
