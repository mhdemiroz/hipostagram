//
//  HGCollectionViewCell.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 05/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import "HGCollectionViewCell.h"

@interface HGCollectionViewCell ()

@property (strong, nonatomic) UIImageView *thumbImage;

@end

@implementation HGCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.thumbImage = [[UIImageView alloc] initWithFrame:frame];
        [self addSubview:self.thumbImage];
        [self.thumbImage autoPinEdgesToSuperviewEdges];
        self.thumbImage.layer.cornerRadius = self.thumbImage.frame.size.width / 2;
        self.thumbImage.layer.masksToBounds = YES;
    }
    return self;
}

- (void)setImageURL:(NSURL *)imageURL {
    [self.thumbImage setImageWithURL:imageURL placeholderImage: [UIImage imageNamed:@"PhotoPlaceholder"]];
}

@end
