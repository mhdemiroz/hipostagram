//
//  HGPhotoDetailViewController.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 06/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import "HGPhotoDetailViewController.h"

@interface HGPhotoDetailViewController ()

@property (strong, nonatomic) UIView *header;
@property (strong, nonatomic) UIImageView *creatorImage;
@property (strong, nonatomic) UILabel *creatorNameLabel;
@property (strong, nonatomic) UILabel *createdDateLabel;
@property (strong, nonatomic) UIImageView *photo;
@property (strong, nonatomic) UIScrollView *photoZoom;

@end

@implementation HGPhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Photo";
    self.navigationController.navigationBar.backgroundColor = [UIColor darkGrayColor];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    // Header
    self.header = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, 60)];
    [self.view addSubview:self.header];
    self.header.backgroundColor = [UIColor lightTextColor];
    [self.header autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height, 0, 0, 0) excludingEdge:ALEdgeBottom];
    [self.header autoSetDimension:ALDimensionHeight toSize:60];
    
    self.creatorImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [self.creatorImage setImageWithURL:self.creatorImageURL placeholderImage:[UIImage imageNamed:@"PhotoPlaceholder"]];
    [self.header addSubview:self.creatorImage];
    [self.creatorImage autoSetDimensionsToSize:CGSizeMake(40, 40)];
    [self.creatorImage autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.header];
    [self.creatorImage autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.header withOffset:10];
    self.creatorImage.layer.cornerRadius = self.creatorImage.frame.size.width / 2;
    self.creatorImage.layer.masksToBounds = YES;
    
    // Creator Name
    self.creatorNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    self.creatorNameLabel.text = self.creatorName;
    self.creatorNameLabel.textAlignment = NSTextAlignmentLeft;
    self.creatorNameLabel.textColor = [UIColor blackColor];
    [self.header addSubview:self.creatorNameLabel];
    [self.creatorNameLabel autoSetDimensionsToSize:CGSizeMake(120, 40)];
    self.creatorNameLabel.adjustsFontSizeToFitWidth = YES;
    self.creatorNameLabel.numberOfLines = 2;
    [self.creatorNameLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.header];
    [self.creatorNameLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:self.creatorImage withOffset:5];
    
    // Created Date
    self.createdDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"MM.dd.yyyy";
    self.createdDateLabel.text = [formatter stringFromDate:self.createdDate];
    self.createdDateLabel.textAlignment = NSTextAlignmentCenter;
    self.createdDateLabel.textColor = [UIColor blackColor];
    [self.header addSubview:self.createdDateLabel];
    [self.createdDateLabel autoSetDimensionsToSize:CGSizeMake(120, 20)];
    [self.createdDateLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.header];
    [self.createdDateLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.header withOffset:10];
    
    // Photo
    self.photoZoom = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.photoZoom.delegate = self;
    self.photoZoom.minimumZoomScale = 1;
    self.photoZoom.maximumZoomScale = 5;
    self.photoZoom.bouncesZoom = NO;
    self.photoZoom.bounces = NO;

    [self.view addSubview:self.photoZoom];
    
    self.photo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PhotoPlaceholder"]];
    self.photo.contentMode = UIViewContentModeScaleAspectFit;
    [self.photo setImageWithURL:self.photoURL];
    self.photoZoom.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    
    [self.photoZoom autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge:ALEdgeTop];
    [self.photoZoom autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.header];
    [self.photoZoom addSubview:self.photo];
    [self.photo autoCenterInSuperview];
    [self.photo autoPinEdgesToSuperviewEdges];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.photo;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if (scale == 1) {
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.photo.frame.size.height);
    }
}

@end
