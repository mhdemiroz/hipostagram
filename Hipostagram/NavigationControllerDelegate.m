//
//  NavigationControllerDelegate.m
//  Hipostagram
//
//  Created by Hakan Demiroz on 07/09/15.
//  Copyright (c) 2015 Hakan Demiroz. All rights reserved.
//

#import "NavigationControllerDelegate.h"

@implementation NavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    if (operation == UINavigationControllerOperationPush) {
        CircleTransitionAnimator *animator = [[CircleTransitionAnimator alloc] init];
        return animator;
    }
    return nil;
}

@end
