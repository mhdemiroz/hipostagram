# README #

Hipostagram is a simple Instagram client that offers the following features:

* Display paginated sets of recent public photos
* Search public photos by tags
* Scroll down to see more photos
* Scroll up to the top to reset
* Tap on a photo to see details and zoom in/out
* Track 3 most recent searches

### Frameworks ###

* [AFNetworking](https://github.com/AFNetworking/AFNetworking)
* [PureLayout](https://github.com/PureLayout/PureLayout)
* [InstagramKit](https://github.com/shyambhat/InstagramKit)
* [CCBottomRefreshControl](https://github.com/vlasov/CCBottomRefreshControl)